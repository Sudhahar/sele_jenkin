package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002CreateLeadTest extends Annotations {
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_LoginAndLogout";
		testcaseDec = "Login into leaftaps";
		author = "koushik";
		category = "smoke";
		excelFileName = "TC001";
	} 
      
	@Test(dataProvider="fetchData")
	  public void createLead(String userName,String Password){
		  new LoginPage()
		  .enterUserName(userName)
		  .enterPassWord(Password)
		  .clickLogin()
		  .clickCRMsfaButton()
		  .clickOnLeadsButton();
		  
	  }
	   
}
