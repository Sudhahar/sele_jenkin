package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations {
	
	   public CreateLeadPage clickCreateLeadButton() {
     	WebElement eleCreateLeads = locateElement("xpath", "//a[text()='Create Lead']");
     	eleCreateLeads.click();
     	return new CreateLeadPage();
     }
    
	
	 
	/*public FindLeadsPage clickFindLeadButton() {
		WebElement eleFindLeads = locateElement("xpath", "//a[text()='Find Leads']");
		eleFindLeads.click();
		return new FindLeadsPage();
	}*/
	
	/*
	public MergeLeadPage clickOnMergeLeadButton() {
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		return new MergeLeadPage();
	}
	*/
}
