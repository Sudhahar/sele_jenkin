package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;



public class ViewLeadPage extends Annotations{
	public ViewLeadPage verifyLoginName() {
		String fName = driver.findElementById("viewLead_firstName_sp").getText();
		if(fName.contains("Sudhahar")) {
			System.out.println("Create Lead success");
		}else {
			System.out.println("Leadname mismatch");
		}
		return this;
	}
	
	/*public EditLeadPage clickOnEditButton() {
		driver.findElementByXPath("//a[text()='Edit']").click();
		return new EditLeadPage();
	}
	
	public DuplicateLeadPage clickOnDuplicateLeadButton() {
		WebElement dl = driver.findElementByXPath("//a[text()='Duplicate Lead']");
		dl.click();
		return new DuplicateLeadPage();
	}
	public ViewLeadPage clickOnDeleteButton() {
 	   driver.findElementByXPath("//a[text()='Delete']").click();
 	   return this;
    }*/
}
