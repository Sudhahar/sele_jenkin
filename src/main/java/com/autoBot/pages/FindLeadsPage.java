package com.autoBot.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.autoBot.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations{
        
	public ViewLeadPage clickOnFirstLeadID() {
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		return new ViewLeadPage();
	}
	
	public FindLeadsPage enterFromLeadId() {
		Set<String> toLead = driver.getWindowHandles();
		List<String> listRefe=new ArrayList<String>();
		listRefe.addAll(toLead);
		
		System.out.println("SecondList"+listRefe);
	    driver.switchTo().window(listRefe.get(1));
	    
	    driver.findElementByXPath("//input[@name='id']").sendKeys("10020");
	    return this;
	}
	    
	    public FindLeadsPage clickOnFromFindLead() throws InterruptedException {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		return this;
	    }
	    
	   /* public MergeLeadPage clickOnFirstlead() {
		driver.switchTo().window(listRefe.get(1));
		Thread.sleep(5000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(listRefe.get(0));
        return new MergeLeadPage();
	}*/

}
