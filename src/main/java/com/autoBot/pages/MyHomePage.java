package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations{
	
           public MyLeadsPage clickOnLeadsButton() {
        	   WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
        	   eleLeads.click();
    	       return new MyLeadsPage();
       }
	

}
