package com.autoBot.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations{
       public CreateLeadPage enterCname(String data) {
    	   driver.findElementById("createLeadForm_companyName").sendKeys(data);
    	   return this;
       }
       public CreateLeadPage enterFname(String data) {
    	   WebElement fn = driver.findElementById("createLeadForm_firstName");
   		fn.sendKeys(data);
   		return this;
       }
       
       public CreateLeadPage enterLname(String data) {
    	   driver.findElementById("createLeadForm_lastName").sendKeys(data);
   		return this;
       }
       
      /* public ViewLeadPage clickSubmit() {
    	   driver.findElementByName("submitButton").sendKeys(Keys.ENTER);
    	   return new ViewLeadPage();
       }
       */
}

